package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  Double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition pos){
    Rectangle2D cell = new Rectangle2D.Double(cellX(pos),cellY(pos),cellWidth(),cellHeight());
    return cell;
  }

  public double cellHeight(){
    double pixelsMargin = (gd.rows()+1) * this.margin;
    double pixelsLeftForCells = box.getHeight() - pixelsMargin;
    return pixelsLeftForCells/gd.rows();
  }

  public double cellWidth(){
    double pixelsMargin = (gd.cols()+1) * this.margin;
    double pixelsLeftForCells = box.getWidth() - pixelsMargin;
    return pixelsLeftForCells/gd.cols();
  }

  public double cellX(CellPosition pos){
    double col = pos.col();
    double xValue = (margin + cellWidth())*(col) + margin + box.getX();
    return xValue;
  }

  public double cellY(CellPosition pos){
    double row = pos.row();
    double yValue = (margin + cellHeight())*(row) + margin + box.getY();
    return yValue;
  }
}
