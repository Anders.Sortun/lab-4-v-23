package no.uib.inf101.gridview;

import java.awt.Dimension;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Arc2D.Double;
import java.util.Iterator;
import java.util.List;
import java.awt.Color;

public class GridView extends JPanel{

  IColorGrid grid;
  // TODO: Implement this class
  public GridView(IColorGrid grid){
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    /*
    Double shape = new Arc2D.Double(
      new Rectangle2D.Double(10, 30, 180, 60),
      15, 270, Arc2D.PIE
    );

    g2.setColor(Color.PINK);
    g2.fill(shape);
    */

    drawGrid(g2);
  }

  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  private static final double INNERMARGIN = 30;

  public void drawGrid(Graphics2D g2){
    //Draw background
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2*OUTERMARGIN;
    double height = this.getHeight() - 2*OUTERMARGIN;
    g2.setColor(MARGINCOLOR);
    Rectangle2D backGround =new Rectangle2D.Double(x,y,width,height);
    g2.fill(backGround);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(backGround, grid, INNERMARGIN);

    drawCells(g2, grid, converter);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection listOfCells, CellPositionToPixelConverter converter){
    Iterator<CellColor> it = listOfCells.getCells().iterator();
    while(it.hasNext()){
      CellColor cell = it.next();
      //Set color for cell
      if (cell.color() == null){
        g2.setColor(Color.LIGHT_GRAY);
        g2.setColor(Color.BLACK);
      } else {
        g2.setColor(cell.color());
      }

      g2.fill(converter.getBoundsForCell(cell.cellPosition()));
    }
  }
}
