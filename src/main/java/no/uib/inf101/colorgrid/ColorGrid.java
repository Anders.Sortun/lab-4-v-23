package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{

  int rows;
  int cols;
  Color standardValue = null;
  CellColor[][] grid;
  
  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    grid = new CellColor[rows][cols];
    for(int i = 0; i < rows; i++){
      for(int j = 0; j < cols; j++){
        grid[i][j] = new CellColor(new CellPosition(i, j), standardValue);
      }
    }
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    // TODO Auto-generated method stub
    List<CellColor> outputList = new ArrayList<>(rows * cols);
    for(int i = 0; i < rows(); i++){
      for(int j = 0; j < cols(); j++){
        outputList.add(grid[i][j]);
      }
    }
    return outputList;
  }

  @Override
  public Color get(CellPosition pos) {
    checkIfOutOfBounds(pos);
    return grid[pos.row()][pos.col()].color();
  }

  @Override
  public void set(CellPosition pos, Color color) {
    checkIfOutOfBounds(pos);
    grid[pos.row()][pos.col()] = new CellColor(pos, color);
  }

  public void checkIfOutOfBounds(CellPosition pos){
    if((pos.row()>this.rows) || (pos.col()>this.cols)){
      throw new IndexOutOfBoundsException();
    }
  }
}
